# This is an example Audioshards heap index file
# ----------------------------------------------
#
# This file, its name is recommended to end on *.ash, relates to Audioshards
# noise file (*.asn) to be downloaded from the first URI in this initial comment
# section (see below).
#
# The heap file content is processed to infer how to interpret the adjacent
# data in the noise file bit by bit. A second URI references a file containing
# metadata of the recording: title, version, edition, artist(s), agency,
# not to forget supported paying methods and according data to use them.
#
# By only downloading the noise file, you get a bunch of bogus, random binary
# data. Actually, byte to byte converting that noise file into wav, you get
# what professional musicians owe their (or rather: no) audience until pay:
# white noise. The heap index file serves as the key in terms of cryptography.
#
# Lines need to be formatted as explained below. Unless prepended by #,
# each line yields to a sequence of frame corrections approaching a target
# value of which the values are inferred from the noise file, slurping chunks
# up to 8 byte length, converting them to big integers and extracting
# smaller ones via divmod() with varying bases. The order and the values of
# these bases are key so the heap index file and the noise file yield
# an audio wav-file of a quality appropriate to the current state of the
# heap index file. So, the purchases of audioshards can be considered
# after listening to the noisy audio reconstructed from what has already
# been purchased for the good of everybody.
#
# Bare numeric values, separated by single space, are defined as follows:
#
#     frames_offset: starting position in the target wave where the values
#       from noise of the audioshard defined by that line will improve
#       precision.
#
#     bottom: the required precision in steps. log2(bottom) must be lower than
#       8*(bytes_per_sample)
#
#     ceil: the precision gain in steps. log2(ceil) must be lower than
#       8*(bytes_per_sample)
#
#     frames_span: for how many adjacent frames the audioshard serve values
#       to approach the original sample values
#
#     rel_bottom_end: positive or negative. added to bottom. All values within
#       bottom and the sum will be linearly spaced and rounded to integer
#
#     rel_ceil_end: positive or negative. added to ceil. All values within ceil
#       and the sum will be linearly spaced and rounded to integer
#
#     price_paid: float with implied currency, plain informational
#       (cf. metadata file). All prices must be proportional to their trapezoid
#       area covered by the audioshard in the time*quality plane of the audio
#       file.
#
#     patron: non-space identifier of person or corporation that paid the price
#       for that audioshard ("patron") for everybody's good.
#
#     patrons_comment: zero to a couple of words, no newline. Can be ads or
#       whatever. Please have in mind that artists or agencies can deny
#       purchase requests due to an inappropriate comment to include.
#
#     hash: hex digest of the chain consisting of checksum of the audioshards
#       noise file (*.asn) and the metadata file (*.asmd), the public key
#       of the providing artist/agent, the hash of the latest fully-purchased
#       audioshard defined before, and the values of all the other fields in
#       the current line.
#
# Once all audioshards are purchased and their reveiled formerly secret key
# figures are listed in the right sequence, the file is a guide for Phoenix
# exactly how to raise from the ashes, i.e. how the original audio data is
# reconstructed into raw-encoded in integer values from the seemingly random
# binary data in the noise file.
# 
# If the adjacent lines to the end of the file are missing or commented, this
# will lead to quantisation noise in the resulting audio file in the concerning
# sections. If there are uncommented lines containing wrong values, or if lines
# before them are missing, the first of these and *any* consequent lines, no
# matter if those are right or wrong, would even yield a considerate share of
# white noise. So be cautious when you insert new lines from e-mails or
# discussion boards, written post-its or alike.
# 
# After the first uncommented line in this file, any commented lines have
# meanings depending on whether they start with `#`, `##', or `###`:
#
#    * With a single `#`, lines with either only three values (bytes to skip,
#      new bit_offset, price to pay) or all values as defined above, from which
#      the former two values can be calculated.
#
#    * With two `#`, Line containing a float denoting the price to pay for the
#      yet-to-purchase audioshard definition to go in this line. Non-number
#      fragments are ignored. Currency must be looked up in the metadata.
#
#    * With three `###`, plain comments with any content left unprocessed by
#      the decoder program.
#
# Non-consecutive lines can be purchased before preceding ones if the audio-
# shards provider renders those with the single-`#` (three-value abbreviation)
# so the algorithm can skip bytes it cannot resolve.
#
# URL of noise file: https://some-domain.tld/path/to/some/file (yet to upload)
# URL of metadata: https://some-domain.tld/metadata/of/my/song (yet to upload)
#
# The metadata is expected to contain the reference to a public-key file. Only
# purchase audioshards from trustworthy sources. Trustworthiness also includes
# that the audioshards provider gives authentic identity information in case
# the material they have encoded in audioshards is equal or too similar to
# music under other copyright.
#
878895 100 7 70510 -13 644 0.28 Fictional_Patron_I knows what he likes! <HASH_NOT_GIVEN>
# 67819 3 0.94 # Fictional_Patron_I 0.72 has not payed that shard completely, only 1.00 in total
1290 87 33 2500 -22 55 0.02 DwnTwnGuy listened to noise from former shard and found the original might turn out likeable <HASH_NOT_GIVEN>
## Give 0.30 for another shard
## Yet another shard costs 0.54.

### By the way: Depending on audioshard provider, this does often not need to be
### money. Emailed or publically posted pictures of contribution receipts of
### charitable organizations may be acknowledged, too. Do not black receiver,
### date and amount, but be welcome to black everything else. The amount
### should be individual in the sense that it can serve as unique criterion with
### date and fund receiver, so realname and address of the contributor can be
### blackened.

### Be assured that the prices are proportional to its time/quality coverage.
### That can be proven by everyone once the shard is purchased. If it turns out
### that some shards are significantly more or less expensive than the average,
### people would blacklist that audioshard provider. The price must be the least
### indicator of how much of the time/quality area is covered by the audioshard.

### No, heap index files in the wild should not (but can) be that verbose. A
### hint that this file is an "Audioshard heap index" in the first line, and
### the URL of the noise file, in a second line, both commented, is enough.
