AudioShards is an algorithm and a surrounding interface to divide
and to reassemble PCM data.

PCM data can be technically regarded as a plane. First dimension
of that plane is the playing time, the second is the quantisation.

Audioshards is also meant as a term for pieces of data that is either
the output or the input of the algorithm.

This also is just a proof-of-concept, once developed likely to reside
as dead text on this platform for good. For a nerd like me, nothing
is more boring than a working program. ;-)

Smart people, however, might be eager to find how to use it for
whatever the purpose, commercial or not, in my humble hope for
the collective good. May you all be successful, at least those
idealistic realists of you who strive to reform music business.


Wherefore?
----------

People I asked (or did not) about their opinion about my idea
replied it is ridiculously utopic. Well anyway, I will honestly
publish it for everybody's inspiration how to do it reasonably
right I guess. If in doubt, just lay it aside until another may
want to try.

I for one just dream of an alternative music business that is driven
by micro-patronage, crowd-funding of *continuously* "better" music
in terms of technical quality, and finally emits creative commons
music for everybody's delight.

In this market, musicians, at least studio musicians, make music
in whatever manner they are used to. They do not need to use the
Sompyler, another project I eagerly developed to understand music
the nerdy way.

But instead of selling licenses or counting on royalties agreed
upon or not really and having them transferred when the business
partners feel inclined, they sell audioshards so they see any
rewards first in the chain.
When they do not get the money calculated, the audience won't enjoy
music in full quality. In that way, risk and pain of not quite
refinanced music is balanced between both creator and audience.

Ideally they would sell each shard only once, without gaps and
overlaps, but the in-betweens and their platforms would not work
and interact very good for the time being. So, regions of the
overall plane may be purchased several times from the musician
or their agency.
If they are fair, to support markets evolving, they would spend that
money they do not deserve according to the ideal, give away
topmost level shards for free. These are in fact hardly marketable
anyway since low quantisation noise at these levels
is quite tolerable to non-audiophile public.

Of course, musicians need to provide a low-quality preview prior
to getting funds in which they should however include slices of
full quality. In fact any linear shapes with any number of breaks
between differently in- or decreasing segments are possible. Shards
can accordingly have any trapezoid form the parallel sides of which
must be vertical, and one parallel side can be zero-length, so
the trapezoid would be a triangle in fact. Reassembling is a bit like
puzzle, which can be fun (not for all, though).

At any rate, they should calculate the total price of a piece to
give freely for good, and take amounts proportional to the size
of the single shard in relation to the entire plane that represents
full playing time and quantisation. Proportional pricing is key
for market transparency.


Copyright & License
-------------------

(C) 2019 Florian 'flowdy' Heß

See LICENSE containing the General public license, version 3.

AudioShards is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Audioshards is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AudioShards. If not, see <http://www.gnu.org/licenses/>.

