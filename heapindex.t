from src.heapindex import HeapIndexFile

hif = HeapIndexFile("example.ash")
print(f"Download noise file at {hif.noise_uri}.")
print(repr(hif.audioshards))
print(repr(hif.outstanding))
