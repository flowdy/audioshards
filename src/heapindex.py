import re
import numpy
from collections import deque
from dataclasses import dataclass

def parse_line(line, numbers_only=False):
    nums = r"[\d?]\S*" if numbers_only else r"\S+"
    return list(m.group(0) for m in re.finditer(r"(?<!\S)" + nums, line))

@dataclass(repr=True)
class AudioShardSpec:
    line_no: int
    frames_offset: int
    bottom: int
    ceil: int
    frames_span: int
    rel_bottom_end: int
    rel_ceil_end: int
    price: float
    patronages: list
    digest: str

    @classmethod
    def parse_from_line(cls, line_no, line):
        if isinstance(line, str):
            values = parse_line(line)
        else:
            values = line
        (frames_offset, bottom, ceil, frames_span,
         rel_bottom_end, rel_ceil_end, price, *remainder) = values
        price = float(price)
        init_data = {
                'frames_offset': int(frames_offset),
                'bottom': int(bottom),
                'ceil': int(ceil),
                'frames_span': int(frames_span),
                'rel_bottom_end': int(rel_bottom_end),
                'rel_ceil_end': int(rel_ceil_end),
                'price': price,
                'patronages': [],
                'digest': None,
            }

        if remainder:

            *comments, digest = remainder
            patronages = " ".join(comments).split(' + ')
            further_patrons = True
            last_patron = None

            for p in patronages:
                name, comment = p.split(" ", 1)
                if not further_patrons: raise RuntimeError(
                    f"Further patrons, but no info about how much paid {last_patron}"
                )
                m = re.match(r"\s*\((\d\S+)\)\s+", comment)
                if m:
                    price = float(m.group(1))
                    comment = comment[m.end:]
                else:
                    price = price - sum(x[1] for x in init_data['patronages'])
                    further_patrons = False
                init_data['patronages'].append((name, price, comment))
                last_patron = name

            if digest == '<HASH_NOT_GIVEN>':
                init_data['digest'] = None
            else:
                init_data['digest'] = int(digest, base=16)

        return cls(line_no, **init_data)

    def divmod_pack_iter(channels):

        bottom = numpy.around(
                numpy.linspace(
                    self.bottom,
                    self.bottom + self.rel_bottom_end,
                    self.frames_span
                )
            )

        height = numpy.around(
                numpy.linspace(
                    self.ceil,
                    self.ceil + self.rel_ceil_end,
                    self.frames_span
                )
            )

        logvals = {}

        offset = -1
        position = 0
        cache = deque()

        former_last_channel = channels

        def height_per_channel():
            nonlocal position
            while reiterate:
                cache_len = len(cache)
                deleted_len = cache[-1][0] - cache_len
                reiterate = False

                while cache_len: # rotate cache
                    pos, steps = cache.popleft()
                    yield pos - deleted_len, steps
                    cache.append(posteps)
                    cache_len -= 1

                position += len(cache)
                for steps in height:
                    for _ in range(channels):
                        for _ in range(former_last_channel % channels):
                            continue
                        position += 1
                        posteps = (position, steps)
                        cache.append(posteps)
                        yield posteps
                        if position == 0:
                            reiterate = True
                            break
                    if reiterate: break

        hpc = height_per_channel()

        while offset < len(bottom)-1:
            logvals.clear()
            position = 0
            logsum = 0
            for i, steps in hpc:
                log2 = numpy.log2(steps)
                if logsum + log2 > 64:
                    break
                else:
                    logsum += log2
                logvals[logsum] = i

            # let the span win which is closest to 2^n
            order = sorted(
                    ( (cnt, *divmod(logsum,1))
                       for logsum, cnt in logvals.items()
                    ), key=lambda x: min(x[2], abs(x[2]-1))
                )

            stuff_ints, bitcount, remainder = order[0]
            for _ in range(stuff_ints):
                cache.popleft()
            if remainder: bitcount += 1

            tail = channels - former_last_channel
            stuff_ints -= tail
            stuff_ints_mask = [True] * tail

            for _ in range(stuff_ints // channels):
               stuff_ints_mask.extend([True] + [False] * (channels-1))

            former_last_channel = stuff_ints % channels
            if former_last_channel:
                stuff_ints_mask.extend(
                        [True] + [False] * (former_last_channel - 1)
                    )

            int_parts = []

            for i in stuff_ints_mask:
                offset += i
                this_bottom = bottom[ offset ]
                this_height = height[ offset ]
                int_parts.append((this_bottom, this_height))

            yield bitcount, int_parts


class HeapIndexFile:

    def __init__(self, filename):

        heapfile = open(filename)

        uris_from_preamble = []

        for line in heapfile:
            if not line.startswith("#"): break
            m = re.search(r"\w+://\S+", line)
            if m:
                uris_from_preamble.append(m.group(0))

        first_line = line
        ( self.noise_uri, self.metadata_uri, *self.further_uris
        ) = uris_from_preamble

        if first_line.startswith("#"):
            raise RuntimeError("No uncommented lines found in file")
        else:
            self.audioshards = [(AudioShardSpec.parse_from_line(1, first_line), True)]

        line_no = 1
        self.is_missing = {}
        for line in heapfile:
            line_no += 1
            if line.startswith("###") or line.isspace(): continue
            elif line.startswith("##"):
                m = re.search(r"\d\S+\d", line)
                if m:
                    self.is_missing[line_no] = (
                            None, None, float(m.group(0))
                        )
                else:
                    breakpoint()
            elif line.startswith("#"):
                values = parse_line(line[1:].split(' # ')[0], True)
                if len(values) == 3:
                    nth_byte, byte_offset, price = values
                    self.is_missing[line_no] = (
                            int(nth_byte),
                            int(byte_offset),
                            float(price)
                        )
                else:
                    self.audioshards.append(
                        (AudioShardSpec.parse_from_line(line_no, values), False)
                    )
            else:
                self.audioshards.append(
                    (AudioShardSpec.parse_from_line(line_no, line), True)
                )
