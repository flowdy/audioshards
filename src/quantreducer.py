from numpy import int, log, ceil

def get_vardepth(steps):
    return int(ceil( log(steps) / log(2) ))

def read_from_binstream(stream, add_max_steps, channels):
    """ [To be implemented] read shard's sample values from output stream """
    vardepth = get_vardepth(add_max_steps)
    # returns numpy array of shape (samples, channels)

def write_to_binstream(stream, add_max_steps, data):
    """ [To be implemented] write shard's sample values to output stream
        data - expected to be a numpy array of shape (samples, channels)
    """
    vardepth = get_vardepth(add_max_steps)

def shard_sample(sample, totalres_byte, base, add_max_steps, addition=None):
    """ Get a shard from sample or set one onto sample given addition 

    >>> shard_sample(6789, 2, 100, 3)
    1
    >>> shard_sample(6789, 2, 0, 100)
    10
    >>> round(10/100*65536)
    6554
    >>> shard_sample(6554, 2, 100, 3, 1)
    6999
    >>> (10+1)/(100+3) * 65536
    6998.9902912621355 # ~> 6999 => q.e.d.
    """

    vardepth = get_vardepth(add_max_steps)

    normbase = sample/2**(totalres_byte*8) * base
    # Cases: vardepth=0, 1, >1
    # - 0: addition is and must be always 0, too.
    if vardepth == 1:
        return 0 if addition is None else sample
    elif vardepth == 2:
        if addition == 1:
            addition += round(normbase) > normbase

    normbase = round(normbase) 
    signbit = 2**(vardepth-1)
    if addition and signbit & addition:
        normbase -= 1
        addition = (2**vardepth-1) & (signbit ^ addition)
    
    upper = base + add_max_steps
    
    if not base:
        base = 1

    if addition is None: # return calculated addition
        return (
                round(sample / 2**(totalres_byte*8) * upper)
              - round(normbase / base * upper)
            )
    else:
        return int(round(
                (round(normbase / base * upper) + addition) / upper
              * 2**(totalres_byte*8)
            ))
