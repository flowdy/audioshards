OFFSET = 1290
START_BOTTOM = 87
START_HEIGHT = 120
LENGTH = 2500
END_BOTTOM = 65
END_HEIGHT = 175

import numpy, soundfile
from src.quantreducer import shard_sample
from math import log2

origwave = soundfile.read("/tmp/anfaengerstueck_1.wav")[0][
        OFFSET: OFFSET + LENGTH
]
bottom = numpy.linspace(START_BOTTOM, END_BOTTOM, LENGTH).round(0)
height = numpy.linspace(START_HEIGHT, END_HEIGHT, LENGTH).round(0)

def waveiter(origwave):

    for left, right in origwave:
        yield left
        yield right

it = waveiter(origwave)
pos = 0
deferred = list()
def next_slice_it():
    for h, d in deferred:
        yield h, d
    for i, sample in enumerate(it):
        this_height = height[pos + i]
        yield log2(this_height), shard_sample(
                sample, 2, int(bottom[pos+i]), int(this_height)
            )

def next_bytes(max_bits=64):
    heights = list()
    shards = list()
    for height, shard in next_slice_it():
        max_bits -= log2(this_height)
        if max_bits < 0:
            break
    try:
        deferred[:] = deferred[2:]
    except IndexError:
        deferred.empty()

    heights = { i: divmod(h, 1) for i, h in enumerate(heights) }
    best_bits = sorted(heights, key=lambda x: heights[x])[-1]

    deferred.extend(...)

print(origwave)
print(height)
