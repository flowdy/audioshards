""" IntSlurper and IntStuffer

    Classes to digest from or write to a binary file sequences of ints.
    The storage of ints is as dense as possible.
    The bitlength N of each integer must be known or stored externally
    and passed to each call of IntSlurper.get_next_int(N) and
    IntStuffer.set_next_int(N, value).
"""

class IntSlurper:
    """ Read ints of known bitlengths from a binary file """

    def __init__(self, filename):
        self.file = open(filename, "rb")
        self.chunk = 0
        self.bit_fill = 0
        self.bytes_read_in_total = 0
        self._fill_chunk()

    def _fill_chunk(self):
        needed_bytes = self.file.read(8 - self.bit_fill // 8)
        if not needed_bytes:
            raise RuntimeError("IntSlurper chunk could not be filled, input stream exhausted.")
        self.bytes_read_in_total += len(needed_bytes)
        self.chunk += int.from_bytes(needed_bytes, 'big')
        self.bit_fill = min(64, self.bytes_read_in_total*8)
    
    def get_next_int(self, bitcount):
        if self.bit_fill < bitcount:
            self._fill_chunk()
        self.bit_fill -= bitcount
        if self.bit_fill < 0:
            raise RuntimeError("Cannot request an integer larger than 64bit")
        slc = self.chunk >> self.bit_fill
        self.chunk = self.chunk << bitcount
        return slc

    def get_integers(self, bases):
        
        bitcount = bitcount_from_bases(bases)
        reduced = self.get_next_int(bitcount)
        
        for base in bases:
            reduced, remainder = divmod(reduced, base)
            yield remainder


class IntStuffer:
    """ Write ints of known upper limit to a binary file. """

    def __init__(self, filename):
        self.file = open(filename, "wb")
        self.chunk = 0
        self.bit_fill = 64

    def set_next_ints(self, values):

        bitcount = bitcount_from_bases(b[0] for b in values)

        if bitcount//8 > 8:
            raise ValueError(f"bitcount > 64")

        int_chunk = 0
        for base, sample in reversed(list(values)):
            int_chunk *= base
            int_chunk += sample

        if not 0 <= int_chunk < 2**bitcount:
            raise ValueError(
                f"{int_chunk} not within integer range of 0..[2^{bitcount}"
            )

        if self.bit_fill < bitcount:
            self.chunk += int_chunk >> self.bit_fill
            bitcount -= self.bit_fill
            self.filename.write(self.chunk.to_bytes(8, 'big'))
            self.bit_fill = 64
            self.chunk = 0

        self.bit_fill -= bitcount
        self.chunk += int_chunk << self.bit_fill

    def finish(self):
        if self.chunk:
            self.chunk >>= (self.bit_fill // 8) * 8
            self.file.write(self.chunk.to_bytes(int((64 - self.bit_fill) / 8 + 0.5), 'big'))
            self.chunk = 0
        self.file.close()


def bitcount_from_bases(bases):
    size = 1
    bitcount = 0

    for b in bases: size *= b
    while size:
        bitcount += 1
        size //= 2

    return bitcount


