xp, yp = map(int, sys.argv[6:1])
bariocentrix(*map(int, sys.argv[:6]))

def bariocentrix(
        xa, ya, diff_ayb, diff_bxc, diff_byc, diff_cyd
    ):

    xa = [xa]
    ya = [ya]
    xb = [xa]
    yb = [ya + diff_ayb]
    xc = [xb + diff_bxc]
    yc = [yb + diff_byc]

    # Fallunterscheidung:
    # xb == xa oder yc == yd: eine Seite hat Länge 0
    #    ^ Dreieck (b, c, d)
    #                   ^ Dreieck (a, b, d)
    # yd > ya und yc < yb: linke Seite beginnt weiter unten und
    #     endet weiter oben als die rechte
    #     3 Dreiecke: (a, ab, d), (ab, c, d), (ab, b, c)
    # ya > yd und yc > yb: rechte Seite beginnt weiter unten und
    #     endet weiter oben als die linke
    #     3 Dreiecke: (a, cd, d), (a, b, cd), (b, c, cd)
    # Linke Seite beginnt weiter unten:
    #     2 Dreiecke: (a, b, d), (b, c, d)
    # Rechte Seite beginnt weiter unten:
    #     2 Dreiecke: (a, c, d), (a, b, c)
    #


    u = (yc - yb) * xp + (xb - xc) * yp + (xc*yb - xb*yc)
    v = (ya - yc) * xp + (xc - xa) * yp + (xa*yc - xc*ya)
    w = (yb - ya) * xp + (xa - xb) * yp + (xb*ya - xa*yb)

    return u, v, w
