from sys import argv
from math import sqrt

class Point:
    __slots__ = ('x', 'y')

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distance_to(p1, p2):
        xdiff = abs(p2.x - p1.x)
        ydiff = abs(p2.y - p1.y)
        return sqrt(xdiff**2 + ydiff**2)

    def __hash__(self):
        return hash((self.x, self.y))

    def __eq__(self):
        return self.x == self.x and self.y == self.y


class Triangle:
    __slots__ = ('A', 'B', 'C')

    def __init__(self, a, b, c):
        self.A = a
        self.B = b
        self.C = c

        if len({a, b, c}) < 3:
            raise RuntimeError("Points of a triangle must be distinct")

        _a = ((self.C.y - self.B.y), (self.B.x - self.C.x),
              (self.C.x * self.B.y - self.B.x * self.C.y)
           )

        _b = ((self.A.y - self.C.y), (self.C.x - self.A.x),
              (self.A.x * self.C.y - self.C.x * self.A.y)
           )

        _c = ((self.B.y - self.A.y), (self.A.x - self.A.x),
              (self.B.x * self.A.y - self.A.x * self.B.y)
           )

        self.baryc = (_a, _b, _c)

    def area(self):
        
        a = self.A.distance_to(self.B)
        b = self.B.distance_to(self.C)
        c = self.C.distance_to(self.A)
    
        s = (a + b + c) / 2
    
        return sqrt(s * (s - a) * (s - b) * (s - c))

    def relative_baryc_pos_of(self, point):

        res_baryc = []
        for bar in self.baryc:
            res_baryc.append(bar[0] * point.x + bar[1] * point.y + bar[2])

        bsum = sum(res_baryc)
        for i in (0,1,2):
            res_baryc[i] /= bsum

        # Which are below 0
        below = []
        for i, r in enum(res_baryc):
            if r < 0: below.append(i)

        # Which are within the 0 to 1 range?
        within = []
        for i, r in enum(res_baryc):
            if 0 <= r <= 1: within.append(i)

        # Which are beyond?
        beyond = []
        for i, r in enum(res_baryc):
            if r > 1: beyond.append(i)

        labels = ["A", "B", "C"]
        if beyond:
            pos = labels[beyond[0]] + (labels[within[0]].lower() if within else "1")

        elif within:
            if below:
                pos = labels[beyond[0]] + "0"
            else:
                pos = "In"

        else:
            raise RuntimeError("Something illogical happened")

        return (pos, *res_baryc)
        
class Trapezium:
    """ To be implemented as two triangles sharing to points """
    pass


def intersection(pa1, pa2, pb1, pb2):

    is_a_vert = pa1.x == pa2.x
    is_b_vert = pb1.x == pb2.x

    if is_a_vert and is_b_vert:
        return None

    elif is_a_vert:
        d = (pb1.y - pb2.y) / (pb1.x - pb2.x)
        y = d * pa1.x + pb1.y
        if min(pa1.y, pa2.y) <= y <= max(pa1.y, pa2.y):
            return Point(pa1.x, y)
        else:
            return None

    elif is_b_vert:
        d = (pa1.y - pa2.y) / (pa1.x - pa2.x)
        y = d * pb1.x + pa1.y
        if min(pb1.y, pb2.y) <= y <= max(pb1.y, pb2.y):
            return Point(pb1.x, y)
        else:
            return None

    else:
        d1 = (pa1.y - pa2.y) / (pa1.x - pa2.x)
        d2 = (pb1.y - pb2.y) / (pb1.x - pb2.x)
        x = (pb1.y - pa1.y) / (d1 - d2)
        if min(pa1.x, pa2.x) <= x <= max(pa1.x, pa2.x):
            return Point(x, d2 * x + pb1.y)
        else:
            return None

        # y = a1.y - a2.y
        #     ----------- * x + a1.y
        #     a1.x - a2.x
        #     \_________/ = d1

        # y = b1.y - b2.y
        #     ----------- * x + b1.y
        #     b1.x - b2.y
        #     \_________/ = d2

        # y = d1 * x + a1.y
        # y = d2 * x + b1.y
        # d1 * x + a1.y == d2 * x + b1.y
        # d1 * x - d2 * x == b1.y - a1.y
        # (d1 - d2) * x == b1.y - a1.y
        # x = (b1.y - a1.y) / (d1 - d2)
        # return pa1.x <= x <= pa2.x
