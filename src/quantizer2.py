""" quantizer.py - Get a shard from an audio file

$ python3 quantizer.py /tmp/anfaengerstueck_1.wav 1290 87 120 2500 65 175 > \
        /tmp/anfaengerstueck1_shardnn.dat
  # 1290 - offset after which to begin (horizontal offset)
  #   87 - start at bottom (vertical offset)
  #  120 - start at height (vertical width)
  # 2500 - number of frames covered (horizontal width)
  #   65 - end of bottom (vertical offset)
  #  175 - end of height (vertical width)

"""

import sys, os, numpy, soundfile
from src.quantreducer import shard_sample
from math import log2
from collections import deque

ORIGWAVE_FN = sys.argv[1]
OFFSET, START_BOTTOM, START_HEIGHT, LENGTH, END_BOTTOM, END_HEIGHT = (
        int(x) for x in sys.argv[2:]
    )
SIZES = (7, 7, 7, 7, 7, 7, 7, 7, 8)

origwave = soundfile.read(ORIGWAVE_FN)[0][
        OFFSET: OFFSET + LENGTH
]
guide = numpy.transpose([
    numpy.rint(numpy.linspace(START_BOTTOM, END_BOTTOM, LENGTH)),
    numpy.rint(numpy.linspace(START_HEIGHT, END_HEIGHT, LENGTH))
])
print(guide)
# vvvv 1st control byte indicates how many bytes need to be digested
# vvvvvvvv
# 0???????                                                                          bis              128
# 10?????? ????????                                                                                16384
# 110????? ???????? ????????                                                                     2097152
# 1110???? ???????? ???????? ????????                                                          268435456
# 11110??? ???????? ???????? ???????? ????????                                               34359738368
# 111110?? ???????? ???????? ???????? ???????? ????????                                    4398046511104
# 1111110? ???????? ???????? ???????? ???????? ???????? ????????                         562949953421312
# 11111110 ???????? ???????? ???????? ???????? ???????? ???????? ????????              72057594037927936
# 11111111 ???????? ???????? ???????? ???????? ???????? ???????? ???????? ????????? 18446744073709551616

def slice_away(steps, values):
    bestsize = (0,0,64)
    for l in range(len(steps)):
        s = sum(log2(i) for i in steps[0:l+1])
        cumsize = 0
        for size in SIZES:
            cumsize += size
            oversize = cumsize - s
            if oversize < 0:
                pass
            else:
                if oversize < bestsize[2]:
                    bestsize = (l, cumsize, oversize)
                break
    total_bytes = bestsize[1] // 7
    length = bestsize[0] + 1
    values = values[:length]
    factor = 1
    total_value = 0
    for i, v in enumerate(reversed(values)):
        total_value += v * factor
        factor *= steps[bestsize[0]-i]
    b = (2**(total_bytes-1)-1)<<(8*total_bytes-(total_bytes-1))
    return length, b + total_value

frame_position = 0
deferred = deque()

def slurp(maxbits):

    incr = 0
    values = []
    while deferred:
        _, height = guide[frame_position + incr]
        maxbits -= log2(height) + origwave.shape[1]-1
        if maxbits < 0:
            break
        values.append((height, deferred.popleft()))
        incr += 1

    while True:
        bottom, height = guide[frame_position + incr]
        maxbits -= log2(height) + origwave.shape[1]-1
        if maxbits < 0:
            break
        for sample in origwave[frame_position + incr]:
            values.append((
                height, shard_sample(sample, 2, int(bottom), int(height))
            ))
        incr += 1

    return values

with os.fdopen(sys.stdout.fileno(), "wb") as stdout:
    while frame_position < LENGTH:

        values = slurp(64)
        incr_pos, bindata = slice_away(
            list(i[0] for i in values), list(i[1] for i in values)
        )
        for x in values[incr_pos:]:
            deferred.appendleft(x)
        frame_position += incr_pos
        stdout.write(bindata.to_bytes())
